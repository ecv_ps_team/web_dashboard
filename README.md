# Bike Prototype Web Dashboard

This frontend React webapp is built by [Serverless Stack](http://serverless-stack.com) 

#### Usage

To use this repo locally, start by cloning it and installing the NPM packages.

``` bash
$ git clone https://bitbucket.org/ecv_global/web_dashboard.git
$ npm install
```

Run it locally.

``` bash
$ npm run start
```

To deploy, replace the following in the [`package.json`](package.json) with your S3 bucket 

```
"deploy": "aws s3 sync build/ s3://xxxxxx",
```

And run.

``` bash
$ npm run deploy
```

