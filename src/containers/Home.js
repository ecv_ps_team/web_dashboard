import React, { Component } from "react";
import { API } from "aws-amplify";
import { Link } from "react-router-dom";
import { Button } from "react-bootstrap";

import "./Home.css";

export default class Home extends Component {
  constructor(props) {
    
    super(props);
    this.state = {};

  }

  async componentDidMount() {

    if (!this.props.isAuthenticated) {
      return;
    }

    this.setState({ isLoading: false });

  }


  renderLander() {
    return (
      <div className="lander">
        <h1>Bike Dashboard Prototype</h1>
        <p>Developed by eCloudvalley</p>
        <div>
          <Link to="/login" className="btn btn-info btn-lg">
            Login
          </Link>
          <Link to="/signup" className="btn btn-success btn-lg">
            Signup
          </Link>
        </div>
      </div>
    );
  }

  renderContents() {
    return (
      <div>
        <Button bsStyle="info" href="/dashboard">Dashboard</Button>{' '}
        <Button bsStyle="info" href="/correlation">Correlation</Button>
      </div>
    );
  }

  render() {
    return (
      <div className="Home">
        {this.props.isAuthenticated ? this.renderContents() : this.renderLander()}
      </div>
    );
  }
}
