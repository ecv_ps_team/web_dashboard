import React, { Component } from "react";
import { Auth } from "aws-amplify";
import { FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import LoaderButton from "../components/LoaderButton";
import "./Login.css";

// Social Login Handler
// import { GoogleLogin, GoogleLogout } from 'react-google-login';
// import FacebookLogin from 'react-facebook-login';

export default class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      email: "",
      password: ""
    };
  }

  validateForm() {
    return this.state.email.length > 0 && this.state.password.length > 0;
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  handleSubmit = async event => {
    event.preventDefault();

    this.setState({ isLoading: true });

    try {
      await Auth.signIn(this.state.email, this.state.password);
      this.props.userHasAuthenticated(true);
    } catch (e) {
      alert(e.message);
      this.setState({ isLoading: false });
    }
  }

 //  responseGoogle = (response) => {
 //      console.log(response);

 //      const user = {
 //        email: response.profileObj.email,
 //        name: response.profileObj.name
 //      };

 //      var federated_res = {
 //        token: response.Zi.id_token,
 //        expires_at: response.Zi.expires_at
 //      }

 //      Auth.federatedSignIn('google', federated_res, user).then(user => {
 //        this.props.userHasAuthenticated(true);
 //        console.log(user);
 //      }).catch(err => {
 //        alert(err.message);
 //        this.setState({ isLoading: false });
 //      });
 //  }

 //  responseFacebook = (response) => {
 //    console.log(response);
 //  }

 //  googleLogOut = () => {
	//   console.log('logout')
	// }

 //  clickFacebookLogin = () => {}

  // social_login = async event => {
  // 	this.setState({ isLoading: true });
  // 	try {
  //     await Auth.federatedSignIn(
  //       'google', {
  //           token: id_token,
  //           expires_at
  //       },
  //     );
  //     this.props.userHasAuthenticated(true);
  //   } catch (e) {
  //     alert(e.message);
  //     this.setState({ isLoading: false });
  //   }
  // }



 //  social_login = async event => {
 //     console.log("test");

 //     const ga = window.gapi.auth2.getAuthInstance();

 //     ga.signIn().then(googleUser => {
 //         const { id_token, expires_at } = googleUser.getAuthResponse();
 //         const profile = googleUser.getBasicProfile();
 //         const user = {
 //             email: profile.getEmail(),
 //             name: profile.getName()
 //         };
 //         try {
 //             Auth.federatedSignIn(
 //                 // Initiate federated sign-in with Google identity provider 
 //                 'google', {
 //                     // the JWT token
 //                     token: id_token,
 //                     // the expiration time
 //                     expires_at
 //                 },
 //                 // a user object
 //                 console.log(user)
 //             ).then(() => {
 //                 console.log("login done");
 //             });
 //         } catch (e) {
 //             alert(e.message);
 //             this.setState({ isLoading: false });
 //         }
 //     });
 // }

  render() {
    return (
      <div className="Login">
        

        <form onSubmit={this.handleSubmit}>
          <FormGroup controlId="email" bsSize="large">
            <ControlLabel>Email</ControlLabel>
            <FormControl
              autoFocus
              type="email"
              value={this.state.email}
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup controlId="password" bsSize="large">
            <ControlLabel>Password</ControlLabel>
            <FormControl
              value={this.state.password}
              onChange={this.handleChange}
              type="password"
            />
          </FormGroup>
          <LoaderButton
            block
            bsSize="large"
            disabled={!this.validateForm()}
            type="submit"
            isLoading={this.state.isLoading}
            text="Login"
            loadingText="Logging in…"
          />
        </form>

        {/* Just in case they want to enable username / password login in future
        <div className="social_login">
          <div className="login_wrapper">
            <GoogleLogin
              clientId="906364071786-c8ic3fq2ej61grm51ng362f89031bp2i.apps.googleusercontent.com"
              buttonText="Login with Google"
              onSuccess={this.responseGoogle}
              onFailure={this.responseGoogle}
            />
          </div>
          <div className="login_wrapper">
            <FacebookLogin
            appId="1918118515167541"
            autoLoad={true}
            fields="name,email,picture"
            onClick={this.clickFacebookLogin}
            callback={this.responseFacebook} />
          </div>
          <div className="clearfix"></div>
        </div>
        */}
      </div>
    );
  }
}
