/*global google*/

import React, { Component } from "react";
import { API } from "aws-amplify";
import { Link } from "react-router-dom";
import { FormGroup, Radio, ControlLabel, FormControl, HelpBlock} from "react-bootstrap";
import * as Datetime from 'react-datetime';
import { compose, withProps, withStateHandlers } from "recompose";
import { withScriptjs, withGoogleMap, GoogleMap, Marker, InfoWindow } from "react-google-maps"
// import InfoBox from "react-google-maps/lib/components/addons/InfoBox";
import HeatmapLayer from "react-google-maps/lib/components/visualization/HeatmapLayer";
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import { v4 as uuid } from 'uuid';
import { moment } from 'moment';
import ReactTags from 'react-tag-autocomplete';

import '../../node_modules/react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import "./Dashboard.css";

const MyMapComponent = compose(
  withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyD6VNLSFY3n-gCDfDCcwmxeKH7eWWU0FsM&v=3.exp&libraries=geometry,drawing,places,visualization",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `100%` }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withScriptjs,
  withGoogleMap
)((props) =>
  <GoogleMap
    defaultZoom={ 14 }
    defaultCenter={{ lat: 51.5045147, lng: -0.1208231 }}
  >
    {props.markers.map((marker, index)=> {
      return (
        <Marker
          key={ uuid() }
          position={{ lat: marker.lat, lng: marker.lng }}
          options={{ icon: { url: "/icons8-bicycle-30.png" } }}
          onClick={( props.onMarkerClick )}
        >
          {marker.show_infowindow &&
          <InfoWindow>
            <div>
              testing
            </div>
          </InfoWindow>
          }
        </Marker>
      )
    })}

    { /* props.isMarkerShown && <Marker position={{ lat: 51.5045147, lng: -0.1208231 }} onClick={props.onMarkerClick} /> */ }
    <HeatmapLayer
      data = { props.heatmapData }
      options = { { radius: 40 } }
    />
  </GoogleMap>
);

export default class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      data_view: 'map',
      show_infowindow: false,
      infobox_data : {},
      bike_usage: [],
      markers: [],
      heatmapData: [],
      district_list: [],
      map_filter: {},
      district_filter: [],
      station_filter: [],
      start_date: "2015-01-04",
      end_date: "2018-03-01",
      holiday_filter: 0,
    };

    this.update_filter = this.update_filter.bind(this);
    this.update_marker = this.update_marker.bind(this);
    // this.handleFilterDelete = this.handleFilterDelete.bind(this);
    // this.handleFilterAdd = this.handleFilterAdd.bind(this);
  }

  async componentDidMount() {

    if (!this.props.isAuthenticated) {
      return;
    }

    // helper function for compare
    function compare(a, b) {
      if (a.usage_sum < b.usage_sum)
        return 1;
      if (a.usage_sum > b.usage_sum)
        return -1;
      return 0;
    }

    function onlyUnique(value, index, self) { 
      return self.indexOf(value) === index;
    }

    try {
      var tmp_district_list = [];
      var district_list = [];
      var station_list = [];
      var heatmapData = [];

      const bike_usage = await API.post("main_api", "/bike_usage", {});

      bike_usage.sort(compare);

      bike_usage.forEach(function(bike_data){
        bike_data.key = bike_data.station_id;
        tmp_district_list.push(bike_data.district);
        station_list.push({ id: bike_data.station_id, name: bike_data.address });
      })

      tmp_district_list = tmp_district_list.filter( onlyUnique );
      
      for(var i=0; i < tmp_district_list.length; i++) {
        district_list.push({ id:i, name:tmp_district_list[i] });
      }

      for(var i=0; i < 10; i++) {
        heatmapData.push({
          location: new google.maps.LatLng(bike_usage[i].lat, bike_usage[i].lon),
          weight: bike_usage[i].usage_sum
        })
      }

      await this.setState({
        bike_usage : bike_usage,
        heatmapData: heatmapData,
        district_list: district_list,
        station_list: station_list,
        show_infowindow: true,
        infowindow_data: {
          lat: bike_usage[0].lat,
          lng: bike_usage[0].lon
        }
      });

      console.log(this.state.infowindow_data);

      this.update_marker();

      console.log("loaded usage data");

    } catch (e) {
      alert(e);
    }

    this.setState({ isLoading: false });

  }


  update_marker(usage_by) {

    if (!usage_by) {
      usage_by = 5;
    }

    var markers = [];
    
    for (var i=0; i < usage_by; i++) {
      if (this.state.bike_usage[i]) {
        markers.push({
          lat: this.state.bike_usage[i].lat,
          lng: this.state.bike_usage[i].lon,
          address: this.state.bike_usage[i].address
        });
      }
    }

    this.setState({
      markers : markers
    });
    
  }


  async update_filter() {
    var new_filter = {
      "date_start": this.state.start_date,
      "date_end": this.state.end_date,
      "holiday_filter": this.state.holiday_filter,
      "district" : [],
      "station_id" : []
    }

    this.state.district_filter.forEach(function(district){
      new_filter.district.push(district.name);
    });

    this.state.station_filter.forEach(function(station){
      console.log(station);
      new_filter.station_id.push(station.id);
    });

    console.log(new_filter);
    // this.update_heat_map_date();

    var heatmapData = [];

    var bike_usage = await API.post("main_api", "/bike_usage", { body: new_filter });

    for(var i=0; i < 10; i++) {
      if (bike_usage[i]) {
        heatmapData.push({
          location: new google.maps.LatLng(bike_usage[i].lat, bike_usage[i].lon),
          weight: bike_usage[i].usage_sum
        })
      }
    }

    this.setState({
      bike_usage : bike_usage,
      heatmapData: heatmapData
    });

    this.update_marker();

  }

  handleMarkerClick(marker) {
    console.log("marker clicked");
    console.log(marker);
    marker.show_infowindow = true;
  }

  renderLander() {
    return (
      <div className="lander">
        <h1>Bike Dashboard Prototype</h1>
        <p>Developed by eCloudvalley</p>
        <div>
          <Link to="/login" className="btn btn-info btn-lg">
            Login
          </Link>
          <Link to="/signup" className="btn btn-success btn-lg">
            Signup
          </Link>
        </div>
      </div>
    );
  }

  renderGoogleMap() {
    return (
      <div className="google_map_container" style={{ height: '500px', width: '100%' }}>
        <MyMapComponent
          markers={this.state.markers}
          onMarkerClick={this.handleMarkerClick}
          heatmapData={this.state.heatmapData}
          show_infowindow={this.state.show_infowindow}
          infowindow_data={this.state.infowindow_data}
        />
      </div>
    );
  }

  renderDataTable() {
    return (
      <div className="data_table_container">
        <BootstrapTable keyField='station_id' data={ this.state.bike_usage } columns={ [{
            dataField: 'station_id',
            text: 'Station ID'
          }, {
            dataField: 'address',
            text: 'Address'
          }, {
            dataField: 'usage_sum',
            text: 'Total Usage'
          }] } pagination={ paginationFactory() } />
        table view
      </div>
    );
  }

  async handleFilterDelete (filter, i) {
    var tags = this.state[filter].slice(0);
    tags.splice(i, 1);
    await this.setState({ [filter]: tags });
    this.update_filter();
  }
 
  async handleFilterAdd (filter, tag) {
    var tags = [].concat(this.state[filter], tag);
    await this.setState({ [filter]: tags });
    this.update_filter();
  }

  renderContents() {
    return (
      <div>
        <div className="filter_box">

          <FormGroup>
            <ControlLabel>View</ControlLabel>
            <div className="clearfix"></div>
            <Radio name="view_by" inline defaultChecked onChange={() => this.setState({data_view:'map'})}>
              Map
            </Radio>{' '}
            <Radio name="view_by" inline onChange={() => this.setState({data_view:'table'})}>
              Table
            </Radio>
          </FormGroup>

          <FormGroup>
            <ControlLabel>Usage by</ControlLabel>
            <div className="clearfix"></div>
            <Radio name="usage_by" inline defaultChecked onChange={() => this.update_marker(5)}>
              Top 5
            </Radio>{' '}
            <Radio name="usage_by" inline onChange={() => this.update_marker(10)}>
              Top 10
            </Radio>{' '}
            <Radio name="usage_by" inline onChange={() => this.update_marker(15)}>
              Top 15
            </Radio>
          </FormGroup>
          
          <FormGroup controlId="start_date">
            <ControlLabel>Start date</ControlLabel>
            <div className="clearfix"></div>
            <Datetime
              defaultValue={new Date(this.state.start_date)}
              dateFormat="YYYY-MM-DD"
              timeFormat={false}
              onChange={async (date) => { await this.setState({ start_date : date.format('YYYY-MM-DD')}); this.update_filter(); }}
            />
          </FormGroup>

          <FormGroup controlId="end_date">
            <ControlLabel>End date</ControlLabel>
            <div className="clearfix"></div>
            <Datetime
              defaultValue={new Date(this.state.end_date)}
              dateFormat="YYYY-MM-DD"
              timeFormat={false}
              onChange={async (date) => { await this.setState({ end_date : date.format('YYYY-MM-DD')}); this.update_filter(); }}
            />
          </FormGroup>

          <FormGroup controlId="district">
            <ControlLabel>District(s)</ControlLabel>
            <ReactTags
              tags={this.state.district_filter}
              suggestions={this.state.district_list}
              handleDelete={this.handleFilterDelete.bind(this, "district_filter")}
              handleAddition={this.handleFilterAdd.bind(this, "district_filter")}
              placeholder="Search District"
            />
          </FormGroup>

          <FormGroup controlId="stations">
            <ControlLabel>Station(s)</ControlLabel>
            <ReactTags
              tags={this.state.station_filter}
              suggestions={this.state.station_list}
              handleDelete={this.handleFilterDelete.bind(this, "station_filter")}
              handleAddition={this.handleFilterAdd.bind(this, "station_filter")}
              placeholder="Search Station"
            />
          </FormGroup>

          <FormGroup>
            <ControlLabel>Public Holiday Filter</ControlLabel>
            <div className="clearfix"></div>
            <Radio name="public_holiday_filter" inline onChange={() => { this.setState.holiday_filter = 1; this.update_filter(); }}>
              On
            </Radio>{' '}
            <Radio name="public_holiday_filter" inline defaultChecked onChange={() => { this.setState.holiday_filter = 0; this.update_filter(); }}>
              Off
            </Radio>
          </FormGroup>

        </div>
        <div className="data_viewer_box">
          {this.state.data_view == 'map' ? this.renderGoogleMap() : this.renderDataTable()}
        </div>
        <div className="clearfix"></div>
      </div>

    );
  }

  render() {
    return (
      <div className="Dashboard">
        {this.props.isAuthenticated ? this.renderContents() : this.renderLander()}
      </div>
    );
  }
}
