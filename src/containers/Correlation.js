import React, { Component } from "react";
import { API } from "aws-amplify";
import { Link } from "react-router-dom";
import { Line } from 'react-chartjs-2';
import { ButtonToolbar, Button } from 'react-bootstrap';
// import { PageHeader, ListGroup, ListGroupItem, Table, FormGroup, Radio, ControlLabel, FormControl, HelpBlock, Button} from "react-bootstrap";

// import "./Home.css";




export default class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      chartData: {}
    };

    this.update_chart_date = this.update_chart_date.bind(this);

  }

  async update_chart_date(data_type) {

    var chart_data = [];
    var chart_title = "";

    if (data_type == 'temperature') {
      chart_title = "Total usage by Temperature (°C) / day"
      var tmp_data = await API.post("main_api", "/correlation", { body: { "type": "by_temperature" } });
      tmp_data.forEach(function(data) {
        chart_data.push({x:data.avg_temp, y:data.usage_sum});
      })
    } else {
      chart_title = "Total usage by Rainfall (mm) / day"
      var tmp_data = await API.post("main_api", "/correlation", { body: { "type": "by_rainfall" } });
      tmp_data.forEach(function(data) {
        chart_data.push({x:data.precip, y:data.usage_sum});
      })
    }

    var data = {
      datasets: [{
        label: chart_title,
        fill: false,
        lineTension: 0.1,
        backgroundColor: 'rgba(75,192,192,0.4)',
        borderColor: 'rgba(75,192,192,1)',
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBorderColor: 'rgba(75,192,192,1)',
        pointBackgroundColor: '#fff',
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: 'rgba(75,192,192,1)',
        pointHoverBorderColor: 'rgba(220,220,220,1)',
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: chart_data
      }]
    }
    this.setState({
      chartData: data
    })
  }

  async componentDidMount() {

    if (!this.props.isAuthenticated) {
      return;
    }

    try {

      const bike_usage = await API.post("main_api", "/bike_usage", {});   
      console.log("loaded usage data");

      var test_data = {
        label: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
        datasets: [65, 59, 80, 81, 56, 55, 40],
        title: "test data set"
      }

      this.update_chart_date('temperature');

    } catch (e) {

      alert(e);

    }

    this.setState({ isLoading: false });
  }


  render() {
    return (
      <div className="Corrleation">
        <div className="">
          Usage By: 
          <ButtonToolbar>
            <Button bsStyle="info" onClick={ () => this.update_chart_date('temperature') }>
              Temperature
            </Button>
            <Button bsStyle="info" onClick={ () => this.update_chart_date('rainfall') }>
              Rainfall
            </Button>
          </ButtonToolbar>

          
        </div>
        <Line
          data={ this.state.chartData }
          options={{
            scales : {
              xAxes: [{
                type: 'linear',
              }]
            }
          }}
        />
      </div>
    );
  }
}
