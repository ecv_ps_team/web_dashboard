export default {
    MAX_ATTACHMENT_SIZE: 5000000,
    s3: {
        REGION: "ap-southeast-1",
        BUCKET: "bike-prototype"
    },
    apiGateway: {
        REGION: "ap-southeast-1",
        // URL: "https://5by75p4gn3.execute-api.us-east-1.amazonaws.com/prod"
        URL: "https://4mid4jw9uc.execute-api.ap-southeast-1.amazonaws.com/dev"
    },
    cognito: {
        REGION: "ap-southeast-1",
        USER_POOL_ID: "ap-southeast-1_N5IOZGBMY",
        APP_CLIENT_ID: "17kh9qlripv3ra0jmj1vp6bmtl",
        IDENTITY_POOL_ID: "ap-southeast-1:70870748-f9d7-45d8-a955-8378f438bead"
    }
};