import React from "react";
import ReactDOM from "react-dom";
import Amplify from "aws-amplify";
// import { Authenticator } from 'aws-amplify-react';
import { BrowserRouter as Router } from "react-router-dom";
import App from "./App";
import registerServiceWorker from "./registerServiceWorker";
import config from "./config";
import "./index.css";

// const AppWithAuth = withAuthenticator(App);

// const federated = {
//     google_client_id: '906364071786-c8ic3fq2ej61grm51ng362f89031bp2i.apps.googleusercontent.com',
//     facebook_app_id: '1918118515167541'
//     // amazon_client_id: ''
// };

Amplify.configure({
    Auth: {
        mandatorySignIn: true,
        region: config.cognito.REGION,
        userPoolId: config.cognito.USER_POOL_ID,
        identityPoolId: config.cognito.IDENTITY_POOL_ID,
        userPoolWebClientId: config.cognito.APP_CLIENT_ID,
        cookieStorage: {
            domain: 'localhost',
            path: '/',
            expires: 7,
            secure: true
        }
    },
    /*Storage: {
      region: config.s3.REGION,
      bucket: config.s3.BUCKET,
      identityPoolId: config.cognito.IDENTITY_POOL_ID
    },*/
    API: {
        endpoints: [{
            name: "main_api",
            endpoint: config.apiGateway.URL,
            region: config.apiGateway.REGION
        }, ]
    }
});


ReactDOM.render(
  <Router>
    <App />
  </Router>,
  document.getElementById("root")
);

registerServiceWorker();
